import typescript from '@rollup/plugin-typescript';
import { resolve } from 'path';
import { defineConfig } from 'vite';

export default defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, 'src/index.ts'),
      name: 'uikit',
      fileName: (format) => `index.${format}.js`,
      formats: ['umd', 'es'],
    },
    sourcemap: true,
    watch: {
      include: 'src/**',
      exclude: 'node_modules/**',
    },
    rollupOptions: {
      plugins: [
        typescript({
          tsconfig: './tsconfig.json',
        }),
      ],
      external: ['react'],
      output: {
        dir: 'dist',
        globals: {
          react: 'React',
        },
      },
    },
  },
  publicDir: false,
  plugins: [],
});
