import React, { PropsWithChildren } from 'react';

import styles from './Card.module.scss';

export type CardProps = PropsWithChildren<{}>;

export const Card = ({ children }: CardProps) => {
  return <div className={styles.card}>{children}</div>;
};
