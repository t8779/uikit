import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';

import { Card } from './Card';

export default {
  title: 'Components/Card',
  component: Card,
} as ComponentMeta<typeof Card>;

const Template: ComponentStory<typeof Card> = (args) => <Card {...args} />;

export const Default = Template.bind({});
Default.args = {
  children: <span>Card</span>,
};

export const MultiRows = Template.bind({});
MultiRows.args = {
  children: (
    <>
      <div>Row 1</div>
      <div>Row 2</div>
      <div>Row 3</div>
    </>
  ),
};

export const WithImage = Template.bind({});
WithImage.args = {
  children: <span>Card</span>,
};
WithImage.parameters = {
  backgrounds: {
    default: 'space',
    values: [
      {
        name: 'space',
        value:
          'url(https://cdn.pixabay.com/photo/2017/08/30/01/05/milky-way-2695569_960_720.jpg)',
      },
    ],
  },
};
