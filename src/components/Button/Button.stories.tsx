import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';

import { Button } from './Button';

export default {
  title: 'Components/Button',
  component: Button,
  argTypes: {
    label: { control: 'label' },
  },
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

export const Default = Template.bind({});
Default.args = {
  label: 'Button',
};

export const WithIcon = Template.bind({});
WithIcon.args = {
  icon: (
    <svg viewBox={`0 0 24 24`} style={{ width: 24, height: 24 }}>
      <rect width={24} height={24} rx='8' />
    </svg>
  ),
  label: 'Button',
};

export const OnlyIcon = Template.bind({});
OnlyIcon.args = {
  icon: (
    <svg viewBox={`0 0 24 24`} style={{ width: 24, height: 24 }}>
      <rect width={24} height={24} rx='8' />
    </svg>
  ),
};
