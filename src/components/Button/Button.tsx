import React, { HTMLAttributes } from 'react';

import styles from './Button.module.scss';

export type ButtonProps = HTMLAttributes<HTMLButtonElement> & {
  label?: string;
  icon?: JSX.Element;
};

export const Button = ({ label, icon, ...props }: ButtonProps) => {
  return (
    <button className={styles.btn} {...props}>
      {icon}
      {label}
    </button>
  );
};
