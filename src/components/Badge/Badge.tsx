import React from 'react';

import styles from './Badge.module.scss';

export type BadgeProps = {
  label: number | string;
};

export const Badge = ({ label }: BadgeProps) => {
  return !label ? null : <span className={styles.badge}>{label}</span>;
};
