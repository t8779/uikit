import { ComponentMeta, ComponentStory } from '@storybook/react';

import { Icon } from './Icon';

export default {
  title: 'Components/Icon',
  component: Icon,
  argTypes: {
    size: { type: 'number' },
  },
} as ComponentMeta<typeof Icon>;

const Template: ComponentStory<typeof Icon> = (args) => <Icon {...args} />;

export const Fallback = Template.bind({});
Fallback.args = {};

export const User = Template.bind({});
User.args = {
  name: 'user',
};
