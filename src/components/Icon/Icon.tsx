import React from 'react';

import styles from './Icon.module.scss';

export type IconProps = {
  name?: string;
  size?: number;
};

export const Icon = ({ name, size = 24 }: IconProps) => {
  return (
    <svg
      viewBox={`0 0 ${size} ${size}`}
      className={styles.icon}
      style={{ width: size, height: size }}
    >
      {name ? (
        <use
          href={`/assets/icons/${name}.svg#${name}`}
          width={size}
          height={size}
        />
      ) : (
        <rect width={size} height={size} rx='8' />
      )}
    </svg>
  );
};
