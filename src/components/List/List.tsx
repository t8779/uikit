import React, { PropsWithChildren } from 'react';

import styles from './List.module.scss';

export type ListProps = PropsWithChildren<{}>;

export const List = ({ children }: ListProps) => {
  return <ul className={styles.list}>{children}</ul>;
};
