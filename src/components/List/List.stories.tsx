import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';

import { List } from './List';

export default {
  title: 'Components/List',
  component: List,
} as ComponentMeta<typeof List>;

const Template: ComponentStory<typeof List> = (args) => <List {...args} />;

export const Default = Template.bind({});
Default.args = {
  children: (
    <>
      <div>Row 1</div>
      <div>Row 2</div>
      <div>Row 3</div>
    </>
  ),
};
